'use strict';

$(document).ready(function(){
    $("#start-computation").click(function() {
        executeComputation();
    });
});

var margin = {top: 20, right: 20, bottom: 30, left: 40},
    width = 500,
    height = 500;

var x = d3.scale.linear()
    .range([0, width]);

var y = d3.scale.linear()
    .range([height, 0]);

var xNorm = d3.scale.linear()
                .range([0.0, 1.0]);
var yNorm = d3.scale.linear()
                .range([0.0, 1.0]);



var _isComputing = false;
var _currentClusteringSublevel = 0;
var _data = [];
var svgDots;
var currentScale = 1.0;
var pc0;
var pcCentroides;
var preparedData = [];
var usedKeys;
var domains = {};

d3.csv('cars.csv', function(error, input) {
    d3.shuffle(input);
    $('#start-computation').prop('disabled', false);
    
    usedKeys = d3.keys(input[0]);
    usedKeys.shift(); // Remove 'name'
    
    usedKeys.forEach(function(k) {
        domains[k] = d3.scale.linear()
                        .range([0.0, 1.0]);
        domains[k].domain(d3.extent(input, function(d) { return +d[k]; }));

    });

    var pI = 1.0 / input.length;
    input.forEach(function(d, i) {
        var newD = {};
        newD.id = i;
        newD.pI = pI;
        usedKeys.forEach(function(k) {
            newD[k] = domains[k](d[k]);
        });
        _data.push(newD);
    });
    console.log(_data);
    pc0 = d3.parcoords()('#pc-all-data')
      .data(input)
        .bundlingStrength(0) // set bundling strength
        .smoothness(0)
        .bundleDimension("cylinders")
        .showControlPoints(false)
        .hideAxis(["name", "id"])
        .render()
        .brushMode("1D-axes")
        .reorderable()
        .interactive();
    pcCentroides = d3.parcoords()('#pc-centroides');
});

function executeComputation() {
    $('#start-computation').prop('disabled', true);
    if (!_isComputing) {
        startComputation();
        $("#num-of-clusters").val('20');
    }
}

var selectedClusters;

function startComputation() {
    _isComputing = true;
    
    var myData = [];
    _data.forEach(function(d, i) {
        myData[i] = {};
        d3.keys(d).forEach(function(k) {
            myData[i][k] = d[k];
        });
        if (selectedClusters !== undefined) {
            myData[i].pI = selectedClusters.pIC[i];
        }
    });

    var shuffledData = _data;//d3.shuffle(_data);

    if (selectedClusters !== undefined) {
        shuffledData = _data.filter(function(i) {
            return selectedClusters.pCI[i.id] > 0.8;
        })
    }

    var centroides = new Array(+$("#num-of-clusters").val());
    for(var c = 0; c < centroides.length; c++) {
        centroides[c] = {
            id: c,
            pC: 1.0 / centroides.length, // p(c)
            pCI: [], // p(c|i)
            pIC: [],
            data: myData,
            selected: false,
            parentCentroid: selectedClusters,
            childrenClass: '',
        };

        usedKeys.forEach(function(k) {
            centroides[c][k] = shuffledData[c][k];    
        });
        
    }

    if (selectedClusters !== undefined) {
        selectedClusters.childrenClass = '.centroid-it-' + _currentClusteringSublevel;
        selectedClusters = undefined;
    }

    var mappedCentrides = new Array(centroides.length);
    // Initialize centroides used to visualize
    centroides.forEach(function(c, i) {
        mappedCentrides[i] = {
            id: c.id,
        };
        usedKeys.forEach(function(k) {
            mappedCentrides[i][k] = domains[k].invert(c[k]);    
        });
    });


    var k = new SoftKMean(myData, +$("#beta-value").val(), centroides, true, usedKeys);

    pcCentroides.data(mappedCentrides).hideAxis(['id']).render()
        .brushMode("1D-axes")
        .interactive();

    usedKeys.forEach(function(k) {
        pcCentroides.scale(k, domains[k].domain());
    }) 

    function iteration(endFunction) { 
        setTimeout(function() { 
            currentIteration++;
            console.log("iteration: " + currentIteration);

            // var prevCentroidesPos = [];
            // svgCentroides.each(function(d, i) {
            //     prevCentroidesPos[i] = { 
            //         x: d3.select(this).attr('cx'), 
            //         y: d3.select(this).attr('cy')
            //     }
            // });

            k.iterate();
            centroides.forEach(function(c, i) {
                usedKeys.forEach(function(k) {
                    mappedCentrides[i][k] = domains[k].invert(c[k]);    
                });
            });

            pcCentroides.render();

            // var hasConverged = true;
            // svgCentroides.each(function(d, i) {
            //     // console.log(Math.abs(prevCentroidesPos[i].x - d3.select(this).attr('cx')) + ',' + Math.abs(prevCentroidesPos[i].y - d3.select(this).attr('cy')))
            //     hasConverged = hasConverged && (Math.abs(prevCentroidesPos[i].x - d3.select(this).attr('cx') < 0.15) 
            //                                 && Math.abs(prevCentroidesPos[i].y - d3.select(this).attr('cy') < 0.15))
            // });

            if (currentIteration > 30) { // || hasConverged) {
                endFunction();
            }
            else { 
                iteration(endFunction); 
            }
        }, 0); 
    }

    var currentIteration = 0;
    iteration(function endFunction() {
        _currentClusteringSublevel++;
        _isComputing = false;
        $('#start-computation').prop('disabled', false);
    });
}

function removeAllSubdivisions(d) {
    if (d.selected) {
        d3.select(this).style('opacity', 1.0);
        d.selected = false;
        svg.selectAll(d.childrenClass).each(removeAllSubdivisions).transition().duration(1000).attr('r', 0).remove();    
    }    
}

function updateCentroidPositionAndRadius(centroides) {
    centroides
          .attr('r', function(d) { return d.radius(); })
          .attr('cx', function(d) { return x(xNorm.invert(d.x)); })
          .attr('cy', function(d) { return y(yNorm.invert(d.y)); })
          
}
function zoomed() {
    svg.select('g.x.axis').call(xAxis);
    svg.select('g.y.axis').call(yAxis);

    currentScale = d3.event.scale;
    
    updateCentroidPositionAndRadius(svg.selectAll('.centroid'));
}

function zoomEnd() {
    svgDots.attr('r', 1.1 * Math.sqrt(currentScale))
      .attr('cx', function(d) { return x(xNorm.invert(d.x)); })
      .attr('cy', function(d) { return y(yNorm.invert(d.y)); })
}
