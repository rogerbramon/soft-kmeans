'use strict';

$(document).ready(function(){
    $("#start-computation").click(function() {
        executeComputation();
    });
});

var margin = {top: 20, right: 20, bottom: 30, left: 40},
    width = 500,
    height = 500;

var x = d3.scale.linear()
    .range([0, width]);

var y = d3.scale.linear()
    .range([height, 0]);

var xNorm = d3.scale.linear()
                .range([0.0, 1.0]);
var yNorm = d3.scale.linear()
                .range([0.0, 1.0]);

var color = d3.scale.category10();

var xAxis = d3.svg.axis()
    .scale(x)
    .orient('bottom');

var yAxis = d3.svg.axis()
    .scale(y)
    .orient('left');

var svg = d3.select('body').append('svg')
    .attr('width', width + margin.left + margin.right)
    .attr('height', height + margin.top + margin.bottom)
  .append('g')
    .attr('width', width)
    .attr('height', height)
    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

var zoom = d3.behavior.zoom()
            .on('zoom', zoomed)
            .on('zoomend', zoomEnd);

var svg2 = d3.select('body').append('svg')
    .attr('width', width + margin.left + margin.right)
    .attr('height', height + margin.top + margin.bottom)
  .append('g')
    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')');

svg.append('g')
      .attr('class', 'x axis')
      .attr('transform', 'translate(0,' + height + ')')
      .call(xAxis)
    .append('text')
      .attr('class', 'label')
      .attr('x', width)
      .attr('y', -6)
      .style('text-anchor', 'end');
  // .text('Sepal Width (cm)');

svg.append('g')
      .attr('class', 'y axis')
      .call(yAxis)
    .append('text')
      .attr('class', 'label')
      .attr('transform', 'rotate(-90)')
      .attr('y', 6)
      .attr('dy', '.71em')
      .style('text-anchor', 'end');
  // .text('Sepal Length (cm)');
svg.append('rect')
        .attr('class', 'pane')
        .attr('width', width)
        .attr('height', height)
        .call(zoom);
svg.append('svg')
        .attr('class', 'working-area')
        .attr('viewbox', '0 0 ' + width + ' ' + height)
        .attr('width', width)
        .attr('height', height);

var _isComputing = false;
var _currentClusteringSublevel = 0;
var _data;
var svgDots;
var currentScale = 1.0;

d3.csv('CoocTexture.csv', function(error, input) {
    _data = d3.shuffle(input);
    $('#start-computation').prop('disabled', false);
});

function executeComputation() {
    $('#start-computation').prop('disabled', true);
    if (!_isComputing) {

        if (_currentClusteringSublevel === 0) {
            initData();
        }
        startComputation();
        $("#num-of-clusters").val('20');
    }
}

function initData() {
    var xAttribute = "10";
    var yAttribute = "11";

    xNorm.domain(d3.extent(_data, function(d) { return +d[xAttribute]; }));
    yNorm.domain(d3.extent(_data, function(d) { return +d[yAttribute]; }));
    x.domain(xNorm.domain()).nice();
    y.domain(yNorm.domain()).nice();
    zoom.x(x);
    zoom.y(y);

    svg.select('g.x.axis').call(xAxis);
    svg.select('g.y.axis').call(yAxis);

    var pI = 1.0 / _data.length;
    _data.forEach(function(d, i) {
        d.id = i;
        d.x = xNorm(+d[xAttribute]);
        d.y = yNorm(+d[yAttribute]);
        d.pIC = [];
        d.pI = pI;
    });

    svgDots = svg2.selectAll('.dot')
          .data(_data, function (d) { return d.id; });
    svgDots.enter().append('circle')
          .attr('class', 'dot')
          .attr('r', 1.1)
          .attr('cx', function(d) { return x(xNorm.invert(d.x)); })
          .attr('cy', function(d) { return y(yNorm.invert(d.y)); })
          .style('fill', function(d) { return color('dot'); });
}

var selectedClusters;

function startComputation() {
    _isComputing = true;
    
    var myData = [];
    _data.forEach(function(d, i) {
        myData[i] = {
            id: d.id,
            x: d.x,
            y: d.y,
            pI: d.pI,
        }

        if (selectedClusters !== undefined) {
            myData[i].pI = selectedClusters.pIC[i];
        }
    });

    

    var shuffledData = _data;//d3.shuffle(_data);

    if (selectedClusters !== undefined) {
        shuffledData = _data.filter(function(i) {
            return selectedClusters.pCI[i.id] > 0.8;
        })
    }
    var centroides = new Array(+$("#num-of-clusters").val());
    for(var c = 0; c < centroides.length; c++) {
        centroides[c] = {
            id: c,
            x: shuffledData[c].x, 
            y: shuffledData[c].y,
            pC: 1.0 / centroides.length, // p(c)
            pCI: [], // p(c|i)
            pIC: [],
            data: myData,
            selected: false,
            parentCentroid: selectedClusters,
            childrenClass: '',
            radius: function () {
                if (this.parentCentroid !== undefined) {
                    return Math.sqrt(this.pC * Math.pow(this.parentCentroid.radius(), 2) + 1);
                }
                else {
                    return Math.sqrt(this.pC * 3000 + 1);
                }
            }
        };
    }

    if (selectedClusters !== undefined) {
        selectedClusters.childrenClass = '.centroid-it-' + _currentClusteringSublevel;
        selectedClusters = undefined;
    }

    var k = new SoftKMean(myData, +$("#beta-value").val(), centroides, false);

    var svgCentroides = svg.select('.working-area').selectAll('.centroid-it-' + _currentClusteringSublevel)
          .data(centroides, function(d) { return d.id; });

    svgCentroides.enter().append('circle')
          .attr('class', 'centroid centroid-it-' + _currentClusteringSublevel)
          
    updateCentroidPositionAndRadius(svgCentroides);

    svgCentroides
          .style('fill', function(d) { return color(_currentClusteringSublevel); })
          .on('mouseenter', function(c) {
            var dotColor = d3.hsl(color('dot'));
            var lightness = dotColor.l;
            svgDots.style('fill', function(i) {
                dotColor.l = lightness + (1 - lightness) * (1 - c.pCI[i.id]);
                return dotColor;
            });
          })
          .on('mouseout', function(d) {
            svgDots.style('fill', color('dot'));
          })
          .on('click', function(d) {
            if (!_isComputing) {
                if (!d.selected) {
                    d3.select(this).style('opacity', 0.5);
                    d.selected = true;
                    selectedClusters = d;
                    executeComputation();
                }
                else {
                    d3.select(this).each(removeAllSubdivisions);
                }
            }

          });

    svgCentroides.exit().remove();

    function iteration(endFunction) { 
        setTimeout(function() { 
            currentIteration++;
            console.log("iteration: " + currentIteration);

            var prevCentroidesPos = [];
            svgCentroides.each(function(d, i) {
                prevCentroidesPos[i] = { 
                    x: d3.select(this).attr('cx'), 
                    y: d3.select(this).attr('cy')
                }
            });

            k.iterate();
            updateCentroidPositionAndRadius(svgCentroides);

            var hasConverged = true;
            svgCentroides.each(function(d, i) {
                // console.log(Math.abs(prevCentroidesPos[i].x - d3.select(this).attr('cx')) + ',' + Math.abs(prevCentroidesPos[i].y - d3.select(this).attr('cy')))
                hasConverged = hasConverged && (Math.abs(prevCentroidesPos[i].x - d3.select(this).attr('cx') < 0.15) && Math.abs(prevCentroidesPos[i].y - d3.select(this).attr('cy') < 0.15))
            });

            if (currentIteration > 1050 || hasConverged) {
                endFunction();
            }
            else { 
                iteration(endFunction); 
            }
        }, 0); 
    }

    var currentIteration = 0;
    iteration(function endFunction() {
        if (centroides[0].parentCentroid !== undefined) {
            var accum = 0.0;
            var parent = Math.pow(centroides[0].parentCentroid.radius(), 2) * Math.PI;
            centroides.forEach(function(d){ 
                accum += Math.pow(d.radius(), 2) * Math.PI;
                console.log(accum + ", " + parent); 
            });
        }
        _currentClusteringSublevel++;
        _isComputing = false;
        $('#start-computation').prop('disabled', false);
    });
}

function removeAllSubdivisions(d) {
    if (d.selected) {
        d3.select(this).style('opacity', 1.0);
        d.selected = false;
        svg.selectAll(d.childrenClass).each(removeAllSubdivisions).transition().duration(1000).attr('r', 0).remove();    
    }    
}

function updateCentroidPositionAndRadius(centroides) {
    centroides
          .attr('r', function(d) { return d.radius(); })
          .attr('cx', function(d) { return x(xNorm.invert(d.x)); })
          .attr('cy', function(d) { return y(yNorm.invert(d.y)); })
          
}
function zoomed() {
    svg.select('g.x.axis').call(xAxis);
    svg.select('g.y.axis').call(yAxis);

    currentScale = d3.event.scale;
    
    updateCentroidPositionAndRadius(svg.selectAll('.centroid'));
}

function zoomEnd() {
    svgDots.attr('r', 1.1 * Math.sqrt(currentScale))
      .attr('cx', function(d) { return x(xNorm.invert(d.x)); })
      .attr('cy', function(d) { return y(yNorm.invert(d.y)); })
}
