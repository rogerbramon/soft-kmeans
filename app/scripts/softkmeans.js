'use strict';

function SoftKMean(data, beta, centroides, isMultidimensionalData, __keys) {
    var _zI = []; // Z(i,ß)
    this.centroides = centroides;
    this.data = data;
    this.beta = beta;
    this.isMultidimensionalData = isMultidimensionalData;
    this.sortedDimensions = {};

    var that = this;
    if (__keys !== undefined) {

        __keys.forEach(function(k) {
                that.sortedDimensions[k] = that.data.slice().sort(function (a, b) {
                                                return a[k] - b[k];
                                            }).map(function(d) {
                                                return d.id;
                                            });                                    
        });
    }

    function distance(Xi, Xc) {
        if (isMultidimensionalData) {
            var result = 0;
            for (var i = 0; i < __keys.length; i++) {
                result += Math.abs(Xi[__keys[i]] - Xc[__keys[i]]);
            }
            return result;
        }
        else {
            var x = Xi.x - Xc.x;
            var y = Xi.y - Xc.y;
            return Math.sqrt(x * x + y * y);
        }
    }

    this.computeZI = function () {
        var dataLength = this.data.length;
        var centridesLength = this.centroides.length;
        for(var i = 0; i < dataLength; i++) {
            var value = 0.0;
            var datum = this.data[i];
            for(var c = 0; c < centridesLength; c++) {
                value += Math.exp(-this.beta * distance(datum, this.centroides[c]));
            }
            _zI[i] = 1.0 / value;
        }
    };

    this.computePropabilities = function () {
        var dataLength = this.data.length;
        var centridesLength = this.centroides.length;
        for(var c = 0; c < centridesLength; c++) {
            var centroide = this.centroides[c];
            var pC = 0.0;
            for(var i = 0; i < dataLength; i++) {
                var datum = this.data[i];
                var pCI = Math.exp(-this.beta * distance(datum, centroide)) * _zI[i];
                centroide.pCI[i] = pCI;
                pC += pCI * datum.pI;
            }
            centroide.pC = pC;
        }
        // console.log(_pC);
        // console.log(_pCI);
    };

    this.nextCentroidesPosition = function () {
        var dataLength = this.data.length;
        var centridesLength = this.centroides.length;
        if (isMultidimensionalData) {
            var propertiesLength = __keys.length;
            var temporalValues = {};
            for (var c = 0; c < centridesLength; c++) {
                var centroide = this.centroides[c];
                for (var x = 0; x < propertiesLength; x++) {
                    var key = __keys[x];
                    temporalValues[key] = 0.0;
                    var orderedValues = this.sortedDimensions[key];
                    var i = 0;
                    var found = false;
                    var acumPCI = 0.0;
                    var median = 0;
                    for(var i = 0; i < dataLength; i++) {
                        var orderedI = orderedValues[i];
                        var datum = this.data[orderedI];
                        var pIC = centroide.pCI[orderedI] * datum.pI / centroide.pC;
                        acumPCI += pIC;
                        centroide.pIC[orderedI] = pIC;

                        if (acumPCI >= 0.5) {
                            if (!found) {
                                median = i;
                                found = true;
                            }
                        }
                    }
                    centroide[key] = this.data[orderedValues[median]][key];
                    // console.log("C: " + c + ", key: " + key + ", " + centroide[key] + ", pC: " + centroide.pC + ", acum: " + acumPCI);
                }
            }
        }
        else {
            for(var c = 0; c < centridesLength; c++) {
                var x = 0.0;
                var y = 0.0;
                var centroide = this.centroides[c];
                // var cpCInv = centroide.pC;
                for(var i = 0; i < dataLength; i++) {
                    var datum = this.data[i];
                    var pIC = centroide.pCI[i] * datum.pI / centroide.pC;
                    centroide.pIC[i] = pIC;
                    x += pIC * datum.x;
                    y += pIC * datum.y;
                }
                centroide.x = x;
                centroide.y = y;
            }
        }
    };
    
    this.iterate = function() {
        // var p = new Parallel(this.data);
        var a = new Date();
        this.computeZI();
        // var b = new Date();
        // console.log("computeZI: " + (b - a));
        this.computePropabilities();
        // var c = new Date();
        // console.log("computePropabilities: " + (c - b));
        this.nextCentroidesPosition();
        var d = new Date();
        // console.log("nextCentroidesPosition: " + (d - c));
        console.log("total: " + (d - a));
        // console.log(this.centroides);
    };

    return this;
}
